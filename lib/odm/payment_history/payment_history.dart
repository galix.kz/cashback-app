import 'package:cashback_app/odm/user/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_odm/cloud_firestore_odm.dart';

import '../firestore_serializable.dart';

// This doesn't exist yet...! See "Next Steps"
part 'payment_history.g.dart';

/// A custom JsonSerializable annotation that supports decoding objects such
/// as Timestamps and DateTimes.
/// This variable can be reused between different models

final paymentHistoryRef = PaymentHistoryCollectionReference();


@Collection<PaymentHistory>('payment_history')
@firestoreSerializable
class PaymentHistory {
  PaymentHistory( {
    required this.id,
    required this.createdAt,
    required this.usedCashBackAmount,
    required this.fullPaymentAmount,
  });

  @Id()
  final String id;
  final DateTime createdAt;
  final int usedCashBackAmount;
  final int fullPaymentAmount;


}

