import 'package:cashback_app/odm/user/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_odm/cloud_firestore_odm.dart';

import '../firestore_serializable.dart';

// This doesn't exist yet...! See "Next Steps"
part 'shop_settings.g.dart';

/// A custom JsonSerializable annotation that supports decoding objects such
/// as Timestamps and DateTimes.
/// This variable can be reused between different models

final shopSettingsRef = ShopSettingsCollectionReference();


@Collection<ShopSettings>('shops')
@firestoreSerializable
class ShopSettings {
  ShopSettings({
    required this.id,
    required this.name,
    required this.cashBackDay,
    required this.cashBackPercentage,
    required this.avatarUrl,
  });

  @Id()
  final String? id;
  final int? cashBackDay;
  final int? cashBackPercentage;
  final String? name;
  final String? avatarUrl;


}

Future<ShopSettings?> getShopByUser(String uid) async {
  try {
    MyUser? user = await MyUser.getUserByUid(uid);
    if (user == null) {
      return null;
    }

    var settingsSnapshot = await shopSettingsRef.whereDocumentId(
        isEqualTo: user.shopId).get();
    if (settingsSnapshot.docs.isNotEmpty) {
      return settingsSnapshot.docs.first.data;
    }
    return null;
  }
  on Exception catch (_) {
    print(_);
  }
}

