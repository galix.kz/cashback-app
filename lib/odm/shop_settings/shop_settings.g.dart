// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'shop_settings.dart';

// **************************************************************************
// CollectionGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, require_trailing_commas, prefer_single_quotes, prefer_double_quotes, use_super_parameters

class _Sentinel {
  const _Sentinel();
}

const _sentinel = _Sentinel();

/// A collection reference object can be used for adding documents,
/// getting document references, and querying for documents
/// (using the methods inherited from Query).
abstract class ShopSettingsCollectionReference
    implements
        ShopSettingsQuery,
        FirestoreCollectionReference<ShopSettings, ShopSettingsQuerySnapshot> {
  factory ShopSettingsCollectionReference([
    FirebaseFirestore? firestore,
  ]) = _$ShopSettingsCollectionReference;

  static ShopSettings fromFirestore(
    DocumentSnapshot<Map<String, Object?>> snapshot,
    SnapshotOptions? options,
  ) {
    return _$ShopSettingsFromJson({'id': snapshot.id, ...?snapshot.data()});
  }

  static Map<String, Object?> toFirestore(
    ShopSettings value,
    SetOptions? options,
  ) {
    return {..._$ShopSettingsToJson(value)}..remove('id');
  }

  @override
  CollectionReference<ShopSettings> get reference;

  @override
  ShopSettingsDocumentReference doc([String? id]);

  /// Add a new document to this collection with the specified data,
  /// assigning it a document ID automatically.
  Future<ShopSettingsDocumentReference> add(ShopSettings value);
}

class _$ShopSettingsCollectionReference extends _$ShopSettingsQuery
    implements ShopSettingsCollectionReference {
  factory _$ShopSettingsCollectionReference([FirebaseFirestore? firestore]) {
    firestore ??= FirebaseFirestore.instance;

    return _$ShopSettingsCollectionReference._(
      firestore.collection('shops').withConverter(
            fromFirestore: ShopSettingsCollectionReference.fromFirestore,
            toFirestore: ShopSettingsCollectionReference.toFirestore,
          ),
    );
  }

  _$ShopSettingsCollectionReference._(
    CollectionReference<ShopSettings> reference,
  ) : super(reference, $referenceWithoutCursor: reference);

  String get path => reference.path;

  @override
  CollectionReference<ShopSettings> get reference =>
      super.reference as CollectionReference<ShopSettings>;

  @override
  ShopSettingsDocumentReference doc([String? id]) {
    assert(
      id == null || id.split('/').length == 1,
      'The document ID cannot be from a different collection',
    );
    return ShopSettingsDocumentReference(
      reference.doc(id),
    );
  }

  @override
  Future<ShopSettingsDocumentReference> add(ShopSettings value) {
    return reference
        .add(value)
        .then((ref) => ShopSettingsDocumentReference(ref));
  }

  @override
  bool operator ==(Object other) {
    return other is _$ShopSettingsCollectionReference &&
        other.runtimeType == runtimeType &&
        other.reference == reference;
  }

  @override
  int get hashCode => Object.hash(runtimeType, reference);
}

abstract class ShopSettingsDocumentReference extends FirestoreDocumentReference<
    ShopSettings, ShopSettingsDocumentSnapshot> {
  factory ShopSettingsDocumentReference(
          DocumentReference<ShopSettings> reference) =
      _$ShopSettingsDocumentReference;

  DocumentReference<ShopSettings> get reference;

  /// A reference to the [ShopSettingsCollectionReference] containing this document.
  ShopSettingsCollectionReference get parent {
    return _$ShopSettingsCollectionReference(reference.firestore);
  }

  @override
  Stream<ShopSettingsDocumentSnapshot> snapshots();

  @override
  Future<ShopSettingsDocumentSnapshot> get([GetOptions? options]);

  @override
  Future<void> delete();

  /// Updates data on the document. Data will be merged with any existing
  /// document data.
  ///
  /// If no document exists yet, the update will fail.
  Future<void> update({
    int? cashBackDay,
    FieldValue cashBackDayFieldValue,
    int? cashBackPercentage,
    FieldValue cashBackPercentageFieldValue,
    String? name,
    FieldValue nameFieldValue,
    String? avatarUrl,
    FieldValue avatarUrlFieldValue,
  });

  /// Updates fields in the current document using the transaction API.
  ///
  /// The update will fail if applied to a document that does not exist.
  void transactionUpdate(
    Transaction transaction, {
    int? cashBackDay,
    FieldValue cashBackDayFieldValue,
    int? cashBackPercentage,
    FieldValue cashBackPercentageFieldValue,
    String? name,
    FieldValue nameFieldValue,
    String? avatarUrl,
    FieldValue avatarUrlFieldValue,
  });
}

class _$ShopSettingsDocumentReference extends FirestoreDocumentReference<
    ShopSettings,
    ShopSettingsDocumentSnapshot> implements ShopSettingsDocumentReference {
  _$ShopSettingsDocumentReference(this.reference);

  @override
  final DocumentReference<ShopSettings> reference;

  /// A reference to the [ShopSettingsCollectionReference] containing this document.
  ShopSettingsCollectionReference get parent {
    return _$ShopSettingsCollectionReference(reference.firestore);
  }

  @override
  Stream<ShopSettingsDocumentSnapshot> snapshots() {
    return reference.snapshots().map(ShopSettingsDocumentSnapshot._);
  }

  @override
  Future<ShopSettingsDocumentSnapshot> get([GetOptions? options]) {
    return reference.get(options).then(ShopSettingsDocumentSnapshot._);
  }

  @override
  Future<ShopSettingsDocumentSnapshot> transactionGet(Transaction transaction) {
    return transaction.get(reference).then(ShopSettingsDocumentSnapshot._);
  }

  Future<void> update({
    Object? cashBackDay = _sentinel,
    FieldValue? cashBackDayFieldValue,
    Object? cashBackPercentage = _sentinel,
    FieldValue? cashBackPercentageFieldValue,
    Object? name = _sentinel,
    FieldValue? nameFieldValue,
    Object? avatarUrl = _sentinel,
    FieldValue? avatarUrlFieldValue,
  }) async {
    assert(
      cashBackDay == _sentinel || cashBackDayFieldValue == null,
      "Cannot specify both cashBackDay and cashBackDayFieldValue",
    );
    assert(
      cashBackPercentage == _sentinel || cashBackPercentageFieldValue == null,
      "Cannot specify both cashBackPercentage and cashBackPercentageFieldValue",
    );
    assert(
      name == _sentinel || nameFieldValue == null,
      "Cannot specify both name and nameFieldValue",
    );
    assert(
      avatarUrl == _sentinel || avatarUrlFieldValue == null,
      "Cannot specify both avatarUrl and avatarUrlFieldValue",
    );
    final json = {
      if (cashBackDay != _sentinel)
        _$ShopSettingsFieldMap['cashBackDay']!: cashBackDay as int?,
      if (cashBackDayFieldValue != null)
        _$ShopSettingsFieldMap['cashBackDay']!: cashBackDayFieldValue,
      if (cashBackPercentage != _sentinel)
        _$ShopSettingsFieldMap['cashBackPercentage']!:
            cashBackPercentage as int?,
      if (cashBackPercentageFieldValue != null)
        _$ShopSettingsFieldMap['cashBackPercentage']!:
            cashBackPercentageFieldValue,
      if (name != _sentinel) _$ShopSettingsFieldMap['name']!: name as String?,
      if (nameFieldValue != null)
        _$ShopSettingsFieldMap['name']!: nameFieldValue,
      if (avatarUrl != _sentinel)
        _$ShopSettingsFieldMap['avatarUrl']!: avatarUrl as String?,
      if (avatarUrlFieldValue != null)
        _$ShopSettingsFieldMap['avatarUrl']!: avatarUrlFieldValue,
    };

    return reference.update(json);
  }

  void transactionUpdate(
    Transaction transaction, {
    Object? cashBackDay = _sentinel,
    FieldValue? cashBackDayFieldValue,
    Object? cashBackPercentage = _sentinel,
    FieldValue? cashBackPercentageFieldValue,
    Object? name = _sentinel,
    FieldValue? nameFieldValue,
    Object? avatarUrl = _sentinel,
    FieldValue? avatarUrlFieldValue,
  }) {
    assert(
      cashBackDay == _sentinel || cashBackDayFieldValue == null,
      "Cannot specify both cashBackDay and cashBackDayFieldValue",
    );
    assert(
      cashBackPercentage == _sentinel || cashBackPercentageFieldValue == null,
      "Cannot specify both cashBackPercentage and cashBackPercentageFieldValue",
    );
    assert(
      name == _sentinel || nameFieldValue == null,
      "Cannot specify both name and nameFieldValue",
    );
    assert(
      avatarUrl == _sentinel || avatarUrlFieldValue == null,
      "Cannot specify both avatarUrl and avatarUrlFieldValue",
    );
    final json = {
      if (cashBackDay != _sentinel)
        _$ShopSettingsFieldMap['cashBackDay']!: cashBackDay as int?,
      if (cashBackDayFieldValue != null)
        _$ShopSettingsFieldMap['cashBackDay']!: cashBackDayFieldValue,
      if (cashBackPercentage != _sentinel)
        _$ShopSettingsFieldMap['cashBackPercentage']!:
            cashBackPercentage as int?,
      if (cashBackPercentageFieldValue != null)
        _$ShopSettingsFieldMap['cashBackPercentage']!:
            cashBackPercentageFieldValue,
      if (name != _sentinel) _$ShopSettingsFieldMap['name']!: name as String?,
      if (nameFieldValue != null)
        _$ShopSettingsFieldMap['name']!: nameFieldValue,
      if (avatarUrl != _sentinel)
        _$ShopSettingsFieldMap['avatarUrl']!: avatarUrl as String?,
      if (avatarUrlFieldValue != null)
        _$ShopSettingsFieldMap['avatarUrl']!: avatarUrlFieldValue,
    };

    transaction.update(reference, json);
  }

  @override
  bool operator ==(Object other) {
    return other is ShopSettingsDocumentReference &&
        other.runtimeType == runtimeType &&
        other.parent == parent &&
        other.id == id;
  }

  @override
  int get hashCode => Object.hash(runtimeType, parent, id);
}

abstract class ShopSettingsQuery
    implements QueryReference<ShopSettings, ShopSettingsQuerySnapshot> {
  @override
  ShopSettingsQuery limit(int limit);

  @override
  ShopSettingsQuery limitToLast(int limit);

  /// Perform an order query based on a [FieldPath].
  ///
  /// This method is considered unsafe as it does check that the field path
  /// maps to a valid property or that parameters such as [isEqualTo] receive
  /// a value of the correct type.
  ///
  /// If possible, instead use the more explicit variant of order queries:
  ///
  /// **AVOID**:
  /// ```dart
  /// collection.orderByFieldPath(
  ///   FieldPath.fromString('title'),
  ///   startAt: 'title',
  /// );
  /// ```
  ///
  /// **PREFER**:
  /// ```dart
  /// collection.orderByTitle(startAt: 'title');
  /// ```
  ShopSettingsQuery orderByFieldPath(
    FieldPath fieldPath, {
    bool descending = false,
    Object? startAt,
    Object? startAfter,
    Object? endAt,
    Object? endBefore,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  });

  /// Perform a where query based on a [FieldPath].
  ///
  /// This method is considered unsafe as it does check that the field path
  /// maps to a valid property or that parameters such as [isEqualTo] receive
  /// a value of the correct type.
  ///
  /// If possible, instead use the more explicit variant of where queries:
  ///
  /// **AVOID**:
  /// ```dart
  /// collection.whereFieldPath(FieldPath.fromString('title'), isEqualTo: 'title');
  /// ```
  ///
  /// **PREFER**:
  /// ```dart
  /// collection.whereTitle(isEqualTo: 'title');
  /// ```
  ShopSettingsQuery whereFieldPath(
    FieldPath fieldPath, {
    Object? isEqualTo,
    Object? isNotEqualTo,
    Object? isLessThan,
    Object? isLessThanOrEqualTo,
    Object? isGreaterThan,
    Object? isGreaterThanOrEqualTo,
    Object? arrayContains,
    List<Object?>? arrayContainsAny,
    List<Object?>? whereIn,
    List<Object?>? whereNotIn,
    bool? isNull,
  });

  ShopSettingsQuery whereDocumentId({
    String? isEqualTo,
    String? isNotEqualTo,
    String? isLessThan,
    String? isLessThanOrEqualTo,
    String? isGreaterThan,
    String? isGreaterThanOrEqualTo,
    bool? isNull,
    List<String>? whereIn,
    List<String>? whereNotIn,
  });
  ShopSettingsQuery whereCashBackDay({
    int? isEqualTo,
    int? isNotEqualTo,
    int? isLessThan,
    int? isLessThanOrEqualTo,
    int? isGreaterThan,
    int? isGreaterThanOrEqualTo,
    bool? isNull,
    List<int?>? whereIn,
    List<int?>? whereNotIn,
  });
  ShopSettingsQuery whereCashBackPercentage({
    int? isEqualTo,
    int? isNotEqualTo,
    int? isLessThan,
    int? isLessThanOrEqualTo,
    int? isGreaterThan,
    int? isGreaterThanOrEqualTo,
    bool? isNull,
    List<int?>? whereIn,
    List<int?>? whereNotIn,
  });
  ShopSettingsQuery whereName({
    String? isEqualTo,
    String? isNotEqualTo,
    String? isLessThan,
    String? isLessThanOrEqualTo,
    String? isGreaterThan,
    String? isGreaterThanOrEqualTo,
    bool? isNull,
    List<String?>? whereIn,
    List<String?>? whereNotIn,
  });
  ShopSettingsQuery whereAvatarUrl({
    String? isEqualTo,
    String? isNotEqualTo,
    String? isLessThan,
    String? isLessThanOrEqualTo,
    String? isGreaterThan,
    String? isGreaterThanOrEqualTo,
    bool? isNull,
    List<String?>? whereIn,
    List<String?>? whereNotIn,
  });

  ShopSettingsQuery orderByDocumentId({
    bool descending = false,
    String startAt,
    String startAfter,
    String endAt,
    String endBefore,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  });

  ShopSettingsQuery orderByCashBackDay({
    bool descending = false,
    int? startAt,
    int? startAfter,
    int? endAt,
    int? endBefore,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  });

  ShopSettingsQuery orderByCashBackPercentage({
    bool descending = false,
    int? startAt,
    int? startAfter,
    int? endAt,
    int? endBefore,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  });

  ShopSettingsQuery orderByName({
    bool descending = false,
    String? startAt,
    String? startAfter,
    String? endAt,
    String? endBefore,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  });

  ShopSettingsQuery orderByAvatarUrl({
    bool descending = false,
    String? startAt,
    String? startAfter,
    String? endAt,
    String? endBefore,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  });
}

class _$ShopSettingsQuery
    extends QueryReference<ShopSettings, ShopSettingsQuerySnapshot>
    implements ShopSettingsQuery {
  _$ShopSettingsQuery(
    this._collection, {
    required Query<ShopSettings> $referenceWithoutCursor,
    $QueryCursor $queryCursor = const $QueryCursor(),
  }) : super(
          $referenceWithoutCursor: $referenceWithoutCursor,
          $queryCursor: $queryCursor,
        );

  final CollectionReference<Object?> _collection;

  @override
  Stream<ShopSettingsQuerySnapshot> snapshots([SnapshotOptions? options]) {
    return reference
        .snapshots()
        .map(ShopSettingsQuerySnapshot._fromQuerySnapshot);
  }

  @override
  Future<ShopSettingsQuerySnapshot> get([GetOptions? options]) {
    return reference
        .get(options)
        .then(ShopSettingsQuerySnapshot._fromQuerySnapshot);
  }

  @override
  ShopSettingsQuery limit(int limit) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.limit(limit),
      $queryCursor: $queryCursor,
    );
  }

  @override
  ShopSettingsQuery limitToLast(int limit) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.limitToLast(limit),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery orderByFieldPath(
    FieldPath fieldPath, {
    bool descending = false,
    Object? startAt = _sentinel,
    Object? startAfter = _sentinel,
    Object? endAt = _sentinel,
    Object? endBefore = _sentinel,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  }) {
    final query =
        $referenceWithoutCursor.orderBy(fieldPath, descending: descending);
    var queryCursor = $queryCursor;

    if (startAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAt: const [],
        startAtDocumentSnapshot: startAtDocument.snapshot,
      );
    }
    if (startAfterDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAfter: const [],
        startAfterDocumentSnapshot: startAfterDocument.snapshot,
      );
    }
    if (endAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        endAt: const [],
        endAtDocumentSnapshot: endAtDocument.snapshot,
      );
    }
    if (endBeforeDocument != null) {
      queryCursor = queryCursor.copyWith(
        endBefore: const [],
        endBeforeDocumentSnapshot: endBeforeDocument.snapshot,
      );
    }

    if (startAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAt: [...queryCursor.startAt, startAt],
        startAtDocumentSnapshot: null,
      );
    }
    if (startAfter != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAfter: [...queryCursor.startAfter, startAfter],
        startAfterDocumentSnapshot: null,
      );
    }
    if (endAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endAt: [...queryCursor.endAt, endAt],
        endAtDocumentSnapshot: null,
      );
    }
    if (endBefore != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endBefore: [...queryCursor.endBefore, endBefore],
        endBeforeDocumentSnapshot: null,
      );
    }
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: query,
      $queryCursor: queryCursor,
    );
  }

  ShopSettingsQuery whereFieldPath(
    FieldPath fieldPath, {
    Object? isEqualTo,
    Object? isNotEqualTo,
    Object? isLessThan,
    Object? isLessThanOrEqualTo,
    Object? isGreaterThan,
    Object? isGreaterThanOrEqualTo,
    Object? arrayContains,
    List<Object?>? arrayContainsAny,
    List<Object?>? whereIn,
    List<Object?>? whereNotIn,
    bool? isNull,
  }) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.where(
        fieldPath,
        isEqualTo: isEqualTo,
        isNotEqualTo: isNotEqualTo,
        isLessThan: isLessThan,
        isLessThanOrEqualTo: isLessThanOrEqualTo,
        isGreaterThan: isGreaterThan,
        isGreaterThanOrEqualTo: isGreaterThanOrEqualTo,
        arrayContains: arrayContains,
        arrayContainsAny: arrayContainsAny,
        whereIn: whereIn,
        whereNotIn: whereNotIn,
        isNull: isNull,
      ),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery whereDocumentId({
    String? isEqualTo,
    String? isNotEqualTo,
    String? isLessThan,
    String? isLessThanOrEqualTo,
    String? isGreaterThan,
    String? isGreaterThanOrEqualTo,
    bool? isNull,
    List<String>? whereIn,
    List<String>? whereNotIn,
  }) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.where(
        FieldPath.documentId,
        isEqualTo: isEqualTo,
        isNotEqualTo: isNotEqualTo,
        isLessThan: isLessThan,
        isLessThanOrEqualTo: isLessThanOrEqualTo,
        isGreaterThan: isGreaterThan,
        isGreaterThanOrEqualTo: isGreaterThanOrEqualTo,
        isNull: isNull,
        whereIn: whereIn,
        whereNotIn: whereNotIn,
      ),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery whereCashBackDay({
    int? isEqualTo,
    int? isNotEqualTo,
    int? isLessThan,
    int? isLessThanOrEqualTo,
    int? isGreaterThan,
    int? isGreaterThanOrEqualTo,
    bool? isNull,
    List<int?>? whereIn,
    List<int?>? whereNotIn,
  }) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.where(
        _$ShopSettingsFieldMap['cashBackDay']!,
        isEqualTo: isEqualTo,
        isNotEqualTo: isNotEqualTo,
        isLessThan: isLessThan,
        isLessThanOrEqualTo: isLessThanOrEqualTo,
        isGreaterThan: isGreaterThan,
        isGreaterThanOrEqualTo: isGreaterThanOrEqualTo,
        isNull: isNull,
        whereIn: whereIn,
        whereNotIn: whereNotIn,
      ),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery whereCashBackPercentage({
    int? isEqualTo,
    int? isNotEqualTo,
    int? isLessThan,
    int? isLessThanOrEqualTo,
    int? isGreaterThan,
    int? isGreaterThanOrEqualTo,
    bool? isNull,
    List<int?>? whereIn,
    List<int?>? whereNotIn,
  }) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.where(
        _$ShopSettingsFieldMap['cashBackPercentage']!,
        isEqualTo: isEqualTo,
        isNotEqualTo: isNotEqualTo,
        isLessThan: isLessThan,
        isLessThanOrEqualTo: isLessThanOrEqualTo,
        isGreaterThan: isGreaterThan,
        isGreaterThanOrEqualTo: isGreaterThanOrEqualTo,
        isNull: isNull,
        whereIn: whereIn,
        whereNotIn: whereNotIn,
      ),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery whereName({
    String? isEqualTo,
    String? isNotEqualTo,
    String? isLessThan,
    String? isLessThanOrEqualTo,
    String? isGreaterThan,
    String? isGreaterThanOrEqualTo,
    bool? isNull,
    List<String?>? whereIn,
    List<String?>? whereNotIn,
  }) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.where(
        _$ShopSettingsFieldMap['name']!,
        isEqualTo: isEqualTo,
        isNotEqualTo: isNotEqualTo,
        isLessThan: isLessThan,
        isLessThanOrEqualTo: isLessThanOrEqualTo,
        isGreaterThan: isGreaterThan,
        isGreaterThanOrEqualTo: isGreaterThanOrEqualTo,
        isNull: isNull,
        whereIn: whereIn,
        whereNotIn: whereNotIn,
      ),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery whereAvatarUrl({
    String? isEqualTo,
    String? isNotEqualTo,
    String? isLessThan,
    String? isLessThanOrEqualTo,
    String? isGreaterThan,
    String? isGreaterThanOrEqualTo,
    bool? isNull,
    List<String?>? whereIn,
    List<String?>? whereNotIn,
  }) {
    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: $referenceWithoutCursor.where(
        _$ShopSettingsFieldMap['avatarUrl']!,
        isEqualTo: isEqualTo,
        isNotEqualTo: isNotEqualTo,
        isLessThan: isLessThan,
        isLessThanOrEqualTo: isLessThanOrEqualTo,
        isGreaterThan: isGreaterThan,
        isGreaterThanOrEqualTo: isGreaterThanOrEqualTo,
        isNull: isNull,
        whereIn: whereIn,
        whereNotIn: whereNotIn,
      ),
      $queryCursor: $queryCursor,
    );
  }

  ShopSettingsQuery orderByDocumentId({
    bool descending = false,
    Object? startAt = _sentinel,
    Object? startAfter = _sentinel,
    Object? endAt = _sentinel,
    Object? endBefore = _sentinel,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  }) {
    final query = $referenceWithoutCursor.orderBy(FieldPath.documentId,
        descending: descending);
    var queryCursor = $queryCursor;

    if (startAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAt: const [],
        startAtDocumentSnapshot: startAtDocument.snapshot,
      );
    }
    if (startAfterDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAfter: const [],
        startAfterDocumentSnapshot: startAfterDocument.snapshot,
      );
    }
    if (endAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        endAt: const [],
        endAtDocumentSnapshot: endAtDocument.snapshot,
      );
    }
    if (endBeforeDocument != null) {
      queryCursor = queryCursor.copyWith(
        endBefore: const [],
        endBeforeDocumentSnapshot: endBeforeDocument.snapshot,
      );
    }

    if (startAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAt: [...queryCursor.startAt, startAt],
        startAtDocumentSnapshot: null,
      );
    }
    if (startAfter != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAfter: [...queryCursor.startAfter, startAfter],
        startAfterDocumentSnapshot: null,
      );
    }
    if (endAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endAt: [...queryCursor.endAt, endAt],
        endAtDocumentSnapshot: null,
      );
    }
    if (endBefore != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endBefore: [...queryCursor.endBefore, endBefore],
        endBeforeDocumentSnapshot: null,
      );
    }

    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: query,
      $queryCursor: queryCursor,
    );
  }

  ShopSettingsQuery orderByCashBackDay({
    bool descending = false,
    Object? startAt = _sentinel,
    Object? startAfter = _sentinel,
    Object? endAt = _sentinel,
    Object? endBefore = _sentinel,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  }) {
    final query = $referenceWithoutCursor.orderBy(
        _$ShopSettingsFieldMap['cashBackDay']!,
        descending: descending);
    var queryCursor = $queryCursor;

    if (startAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAt: const [],
        startAtDocumentSnapshot: startAtDocument.snapshot,
      );
    }
    if (startAfterDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAfter: const [],
        startAfterDocumentSnapshot: startAfterDocument.snapshot,
      );
    }
    if (endAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        endAt: const [],
        endAtDocumentSnapshot: endAtDocument.snapshot,
      );
    }
    if (endBeforeDocument != null) {
      queryCursor = queryCursor.copyWith(
        endBefore: const [],
        endBeforeDocumentSnapshot: endBeforeDocument.snapshot,
      );
    }

    if (startAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAt: [...queryCursor.startAt, startAt],
        startAtDocumentSnapshot: null,
      );
    }
    if (startAfter != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAfter: [...queryCursor.startAfter, startAfter],
        startAfterDocumentSnapshot: null,
      );
    }
    if (endAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endAt: [...queryCursor.endAt, endAt],
        endAtDocumentSnapshot: null,
      );
    }
    if (endBefore != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endBefore: [...queryCursor.endBefore, endBefore],
        endBeforeDocumentSnapshot: null,
      );
    }

    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: query,
      $queryCursor: queryCursor,
    );
  }

  ShopSettingsQuery orderByCashBackPercentage({
    bool descending = false,
    Object? startAt = _sentinel,
    Object? startAfter = _sentinel,
    Object? endAt = _sentinel,
    Object? endBefore = _sentinel,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  }) {
    final query = $referenceWithoutCursor.orderBy(
        _$ShopSettingsFieldMap['cashBackPercentage']!,
        descending: descending);
    var queryCursor = $queryCursor;

    if (startAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAt: const [],
        startAtDocumentSnapshot: startAtDocument.snapshot,
      );
    }
    if (startAfterDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAfter: const [],
        startAfterDocumentSnapshot: startAfterDocument.snapshot,
      );
    }
    if (endAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        endAt: const [],
        endAtDocumentSnapshot: endAtDocument.snapshot,
      );
    }
    if (endBeforeDocument != null) {
      queryCursor = queryCursor.copyWith(
        endBefore: const [],
        endBeforeDocumentSnapshot: endBeforeDocument.snapshot,
      );
    }

    if (startAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAt: [...queryCursor.startAt, startAt],
        startAtDocumentSnapshot: null,
      );
    }
    if (startAfter != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAfter: [...queryCursor.startAfter, startAfter],
        startAfterDocumentSnapshot: null,
      );
    }
    if (endAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endAt: [...queryCursor.endAt, endAt],
        endAtDocumentSnapshot: null,
      );
    }
    if (endBefore != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endBefore: [...queryCursor.endBefore, endBefore],
        endBeforeDocumentSnapshot: null,
      );
    }

    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: query,
      $queryCursor: queryCursor,
    );
  }

  ShopSettingsQuery orderByName({
    bool descending = false,
    Object? startAt = _sentinel,
    Object? startAfter = _sentinel,
    Object? endAt = _sentinel,
    Object? endBefore = _sentinel,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  }) {
    final query = $referenceWithoutCursor
        .orderBy(_$ShopSettingsFieldMap['name']!, descending: descending);
    var queryCursor = $queryCursor;

    if (startAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAt: const [],
        startAtDocumentSnapshot: startAtDocument.snapshot,
      );
    }
    if (startAfterDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAfter: const [],
        startAfterDocumentSnapshot: startAfterDocument.snapshot,
      );
    }
    if (endAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        endAt: const [],
        endAtDocumentSnapshot: endAtDocument.snapshot,
      );
    }
    if (endBeforeDocument != null) {
      queryCursor = queryCursor.copyWith(
        endBefore: const [],
        endBeforeDocumentSnapshot: endBeforeDocument.snapshot,
      );
    }

    if (startAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAt: [...queryCursor.startAt, startAt],
        startAtDocumentSnapshot: null,
      );
    }
    if (startAfter != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAfter: [...queryCursor.startAfter, startAfter],
        startAfterDocumentSnapshot: null,
      );
    }
    if (endAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endAt: [...queryCursor.endAt, endAt],
        endAtDocumentSnapshot: null,
      );
    }
    if (endBefore != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endBefore: [...queryCursor.endBefore, endBefore],
        endBeforeDocumentSnapshot: null,
      );
    }

    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: query,
      $queryCursor: queryCursor,
    );
  }

  ShopSettingsQuery orderByAvatarUrl({
    bool descending = false,
    Object? startAt = _sentinel,
    Object? startAfter = _sentinel,
    Object? endAt = _sentinel,
    Object? endBefore = _sentinel,
    ShopSettingsDocumentSnapshot? startAtDocument,
    ShopSettingsDocumentSnapshot? endAtDocument,
    ShopSettingsDocumentSnapshot? endBeforeDocument,
    ShopSettingsDocumentSnapshot? startAfterDocument,
  }) {
    final query = $referenceWithoutCursor
        .orderBy(_$ShopSettingsFieldMap['avatarUrl']!, descending: descending);
    var queryCursor = $queryCursor;

    if (startAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAt: const [],
        startAtDocumentSnapshot: startAtDocument.snapshot,
      );
    }
    if (startAfterDocument != null) {
      queryCursor = queryCursor.copyWith(
        startAfter: const [],
        startAfterDocumentSnapshot: startAfterDocument.snapshot,
      );
    }
    if (endAtDocument != null) {
      queryCursor = queryCursor.copyWith(
        endAt: const [],
        endAtDocumentSnapshot: endAtDocument.snapshot,
      );
    }
    if (endBeforeDocument != null) {
      queryCursor = queryCursor.copyWith(
        endBefore: const [],
        endBeforeDocumentSnapshot: endBeforeDocument.snapshot,
      );
    }

    if (startAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAt: [...queryCursor.startAt, startAt],
        startAtDocumentSnapshot: null,
      );
    }
    if (startAfter != _sentinel) {
      queryCursor = queryCursor.copyWith(
        startAfter: [...queryCursor.startAfter, startAfter],
        startAfterDocumentSnapshot: null,
      );
    }
    if (endAt != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endAt: [...queryCursor.endAt, endAt],
        endAtDocumentSnapshot: null,
      );
    }
    if (endBefore != _sentinel) {
      queryCursor = queryCursor.copyWith(
        endBefore: [...queryCursor.endBefore, endBefore],
        endBeforeDocumentSnapshot: null,
      );
    }

    return _$ShopSettingsQuery(
      _collection,
      $referenceWithoutCursor: query,
      $queryCursor: queryCursor,
    );
  }

  @override
  bool operator ==(Object other) {
    return other is _$ShopSettingsQuery &&
        other.runtimeType == runtimeType &&
        other.reference == reference;
  }

  @override
  int get hashCode => Object.hash(runtimeType, reference);
}

class ShopSettingsDocumentSnapshot
    extends FirestoreDocumentSnapshot<ShopSettings> {
  ShopSettingsDocumentSnapshot._(this.snapshot) : data = snapshot.data();

  @override
  final DocumentSnapshot<ShopSettings> snapshot;

  @override
  ShopSettingsDocumentReference get reference {
    return ShopSettingsDocumentReference(
      snapshot.reference,
    );
  }

  @override
  final ShopSettings? data;
}

class ShopSettingsQuerySnapshot extends FirestoreQuerySnapshot<ShopSettings,
    ShopSettingsQueryDocumentSnapshot> {
  ShopSettingsQuerySnapshot._(
    this.snapshot,
    this.docs,
    this.docChanges,
  );

  factory ShopSettingsQuerySnapshot._fromQuerySnapshot(
    QuerySnapshot<ShopSettings> snapshot,
  ) {
    final docs =
        snapshot.docs.map(ShopSettingsQueryDocumentSnapshot._).toList();

    final docChanges = snapshot.docChanges.map((change) {
      return _decodeDocumentChange(
        change,
        ShopSettingsDocumentSnapshot._,
      );
    }).toList();

    return ShopSettingsQuerySnapshot._(
      snapshot,
      docs,
      docChanges,
    );
  }

  static FirestoreDocumentChange<ShopSettingsDocumentSnapshot>
      _decodeDocumentChange<T>(
    DocumentChange<T> docChange,
    ShopSettingsDocumentSnapshot Function(DocumentSnapshot<T> doc) decodeDoc,
  ) {
    return FirestoreDocumentChange<ShopSettingsDocumentSnapshot>(
      type: docChange.type,
      oldIndex: docChange.oldIndex,
      newIndex: docChange.newIndex,
      doc: decodeDoc(docChange.doc),
    );
  }

  final QuerySnapshot<ShopSettings> snapshot;

  @override
  final List<ShopSettingsQueryDocumentSnapshot> docs;

  @override
  final List<FirestoreDocumentChange<ShopSettingsDocumentSnapshot>> docChanges;
}

class ShopSettingsQueryDocumentSnapshot
    extends FirestoreQueryDocumentSnapshot<ShopSettings>
    implements ShopSettingsDocumentSnapshot {
  ShopSettingsQueryDocumentSnapshot._(this.snapshot) : data = snapshot.data();

  @override
  final QueryDocumentSnapshot<ShopSettings> snapshot;

  @override
  final ShopSettings data;

  @override
  ShopSettingsDocumentReference get reference {
    return ShopSettingsDocumentReference(snapshot.reference);
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ShopSettings _$ShopSettingsFromJson(Map<String, dynamic> json) => ShopSettings(
      id: json['id'] as String?,
      name: json['name'] as String?,
      cashBackDay: json['cashBackDay'] as int?,
      cashBackPercentage: json['cashBackPercentage'] as int?,
      avatarUrl: json['avatarUrl'] as String?,
    );

const _$ShopSettingsFieldMap = <String, String>{
  'id': 'id',
  'cashBackDay': 'cashBackDay',
  'cashBackPercentage': 'cashBackPercentage',
  'name': 'name',
  'avatarUrl': 'avatarUrl',
};

Map<String, dynamic> _$ShopSettingsToJson(ShopSettings instance) =>
    <String, dynamic>{
      'id': instance.id,
      'cashBackDay': instance.cashBackDay,
      'cashBackPercentage': instance.cashBackPercentage,
      'name': instance.name,
      'avatarUrl': instance.avatarUrl,
    };
