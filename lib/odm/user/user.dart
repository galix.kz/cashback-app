import 'package:cashback_app/odm/cashback/cashback.dart';
import 'package:cashback_app/odm/payment_history/payment_history.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_odm/cloud_firestore_odm.dart';

import '../firestore_serializable.dart';
import '../shop_settings/shop_settings.dart';

// This doesn't exist yet...! See "Next Steps"
part 'user.g.dart';

/// A custom JsonSerializable annotation that supports decoding objects such
/// as Timestamps and DateTimes.
/// This variable can be reused between different models

enum UserType { shop, client }

@Collection<MyUser>('users')
@firestoreSerializable
class MyUser {
  MyUser(
      {required this.userType,
      required this.id,
      this.email,
      this.phone,
      this.fullName,
      this.shopId});

  @Id()
  final String id;

  final String? fullName;
  final String? phone;
  final String? email;
  final UserType userType;
  final String? shopId;

  Future<ShopSettings?> shopSettings() async {
    if (shopId != null) {
      var settingsSnapshot =
          await shopSettingsRef.whereDocumentId(isEqualTo: shopId!).get();
      if (settingsSnapshot.docs.isNotEmpty) {
        return settingsSnapshot.docs.first.data;
      }
    }
    return null;
  }

  Future<void> useCashback(
      {required String shopId,
      required int usedCashBackValue,
      required int paymentFullAmount}) async {
    WriteBatch batch = FirebaseFirestore.instance.batch();
    var cashbackSnapshot = await cashBackRef
        .whereUserId(isEqualTo: id!)
        .whereIsUsed(isEqualTo: false)
        .whereShopId(isEqualTo: shopId!)
        .get();
    if (cashbackSnapshot.docs.isNotEmpty) {
      for (var element in cashbackSnapshot.docs) {
        batch.update(element.reference.reference,
            {"isUsed": true, "usedAt": DateTime.now()});
      }
      batch.commit();

      var paymentHistorySnapshot = paymentHistoryRef.doc();
      paymentHistorySnapshot.set(PaymentHistory(id: paymentHistorySnapshot.id,
          createdAt: DateTime.now(), usedCashBackAmount: usedCashBackValue, fullPaymentAmount: paymentFullAmount));

      return;
    }
  }

  Future createCashBack(
      {required String shopId,
      required int cashBackValue,
      required int paymentFullAmount,
      required int cashBackDay}) async {
    var cashback = cashBackRef.doc();
    cashback.set(CashBack(
      id: cashback.id,
      shopId: shopId,
      userId: id,
      isUsed: false,
      createdAt: DateTime.now(),
      availableAt: DateTime.now().add(Duration(days: cashBackDay)),
      cashBackSum: cashBackValue,
      paymentFullAmount: paymentFullAmount,
    ));
    var paymentHistorySnapshot = paymentHistoryRef.doc();
    paymentHistorySnapshot.set(PaymentHistory(id: paymentHistorySnapshot.id,
        createdAt: DateTime.now(), usedCashBackAmount: 0, fullPaymentAmount: paymentFullAmount));

  }

  static getUserByUid(String uid) async{
    try {
      MyUserQuerySnapshot userData = await usersRef.whereDocumentId(isEqualTo: uid).get();
      if (userData.docs.isEmpty) {
        return null;
      }
      MyUser user = userData.docs.first.data;
      return user;
    } on Exception catch (_) {
      print(_);
    }
  }
}

final usersRef = MyUserCollectionReference();
