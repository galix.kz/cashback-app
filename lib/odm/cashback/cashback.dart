import 'package:cashback_app/odm/user/user.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore_odm/cloud_firestore_odm.dart';

import '../firestore_serializable.dart';

// This doesn't exist yet...! See "Next Steps"
part 'cashback.g.dart';

/// A custom JsonSerializable annotation that supports decoding objects such
/// as Timestamps and DateTimes.
/// This variable can be reused between different models

final cashBackRef = CashBackCollectionReference();


@Collection<CashBack>('cashback')
@firestoreSerializable
class CashBack {
  CashBack({
    required this.id,
    required this.shopId,
    required this.userId,
    required this.isUsed,
    required this.createdAt,
    required this.availableAt,
    required this.cashBackSum,
    required this.paymentFullAmount,
    this.canceledAt,
    this.usedAt,

  });

  @Id()
  final String? id;
  final String shopId;
  final String userId;
  final bool isUsed;
  final DateTime createdAt;
  final DateTime? usedAt;
  final DateTime? canceledAt;
  final DateTime availableAt;
  final int cashBackSum;
  final int paymentFullAmount;

  static Future<int> getCashbackBalanceByUserId(
      {required String clientId, required String shopId}) async {
    try {
       var cashbacks = await cashBackRef.whereUserId(isEqualTo: clientId).whereIsUsed(isEqualTo: false).whereShopId(isEqualTo: shopId).whereAvailableAt(isLessThanOrEqualTo: DateTime.now()).get();
       int sum = 0;

       for (var cashback in cashbacks.docs) {
         sum += cashback.data.cashBackSum;
       }
       print(sum);
      return sum;
    }
    on Exception catch (_) {
      print(_);
    }
    return 0;
  }

  static Future<Map<String, int>> getUsersAllCashBacks(
      {required String clientId}) async {
    try {
      var cashbacks = await cashBackRef.whereUserId(isEqualTo: clientId).whereIsUsed(isEqualTo: false).whereAvailableAt(isLessThanOrEqualTo: DateTime.now()).get();
      Map<String, int> all = {};
      for (var cashback in cashbacks.docs) {
        if(all[cashback.data.shopId]==null) {
          all[cashback.data.shopId] = 0;
        }
        all[cashback.data.shopId] = all[cashback.data.shopId]! + cashback.data.cashBackSum;
      }
      return all;
    }
    on Exception catch (_) {
      print(_);
    }
    return {};
  }


}

