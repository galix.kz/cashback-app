import 'dart:io';

import 'package:cashback_app/screens/client/login/login_with_phone.dart';
import 'package:cashback_app/screens/client/main_client_screen/main_client_screen.dart';
import 'package:cashback_app/screens/shop/finish_payment_screen.dart';
import 'package:cashback_app/screens/shop/history_screen/history_screen.dart';
import 'package:cashback_app/screens/shop/numpad_screen.dart';
import 'package:cashback_app/screens/shop/qr_reader_screen.dart';
import 'package:cashback_app/screens/shop/setting_shop_screen.dart';
import 'package:cashback_app/screens/shop/shop_login_screen.dart';
import 'package:cashback_app/screens/splash_screen.dart';
import 'package:cashback_app/utils/guard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:firebase_core/firebase_core.dart';

import 'screens/login_redirect_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  if(kIsWeb){
    await Firebase.initializeApp(options: FirebaseOptions(apiKey: "AIzaSyB7ZwU8fWFKA-Nfkg0rvYCHYYHt-x25f2Q"
      , appId:  "1:598711147826:web:319e03a5737da8bb370b2b",
      messagingSenderId: "598711147826", projectId: "galix-cashback", authDomain: "galix-cashback.firebaseapp.com",     storageBucket: "galix-cashback.appspot.com",));
  }
  else{
    await Firebase.initializeApp();
  }

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Q app',
      onGenerateRoute: RouteGuard.onGenerateRoute,
      initialRoute: '/',
      routes: {
        '/': (context) => SplashScreen(),
        '/shop': (context) => NumPadScreen(),
        '/shop/settings': (context) => ShopSettingScreen(),
        '/shop/qr_reader': (context) => QrReaderScreen(),
        '/shop/finish_payment': (context) => FinishPaymentScreen(),
        '/shop/history': (context) => HistoryScreen(),
        '/client': (context) => MainClientScreen(),
        '/login': (context) => LoginRedirectScreen(),
        '/login/shop': (context) => ShopLoginScreen(),
        '/login/client': (context) => LoginWithPhone(),
      },
    );
  }
}
