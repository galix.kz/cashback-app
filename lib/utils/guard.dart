import 'package:cashback_app/screens/login_redirect_screen.dart';
import 'package:cashback_app/screens/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

class RouteGuard {
  static Route<dynamic>? onGenerateRoute(RouteSettings settings) {
    final user = FirebaseAuth.instance.currentUser;
    print(settings.name!.contains('/login'));
    if (user == null) {

      if (settings.name != '/login') {
        // Redirect to login page if user is not authenticated
        return MaterialPageRoute(builder: (_) => const LoginRedirectScreen(), settings: const RouteSettings(
          name: 'login',
        ));
      }
    } else {
      if (settings.name == '/login') {
        // Redirect to home page if user is already authenticated
        return MaterialPageRoute(builder: (_) => SplashScreen());
      }
    }

    // If no redirection is necessary, simply return null
    return null;
  }
}
