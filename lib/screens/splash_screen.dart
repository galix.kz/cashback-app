import 'dart:async';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';

import '../odm/user/user.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<User?>(
      future: Future.value(FirebaseAuth.instance.currentUser),
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
              Timer(const Duration(milliseconds: 500), () {
                Navigator.popAndPushNamed(context, '/login');
              });
          } else {
            MyUser.getUserByUid(snapshot.data!.uid).then((user) => {
                if(user?.userType ==UserType.client){
                  Navigator.popAndPushNamed(context, '/client')
                }
                else if(user?.userType == UserType.shop){
                  Navigator.popAndPushNamed(context, '/shop')
                }
              }
            );
          }
        }
        return const Scaffold(
          body: Center(
            child: CircularProgressIndicator(),
          ),
        );
      },
    );
  }
}
