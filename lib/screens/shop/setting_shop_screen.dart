import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';

import '../../odm/shop_settings/shop_settings.dart';

class ShopSettingScreen extends StatefulWidget {
  const ShopSettingScreen({Key? key}) : super(key: key);

  @override
  _ShopSettingPageState createState() => _ShopSettingPageState();
}

class _ShopSettingPageState extends State<ShopSettingScreen> {
  final _formKey = GlobalKey<FormState>();
  final shopNameController = TextEditingController();
  String _uid = '';
  double _cashbackDay = 1.0;
  int _cashbackPercentage = 10;
  File? _image;
  String? defaultImage = '';
  bool isSaving = false;
  @override
  void initState() {
    super.initState();
    _loadData();
  }

  Future<void> _loadData() async {
    // Load shop settings from Firestore
    final user = FirebaseAuth.instance.currentUser;
    if (user == null) return;
    ShopSettings? shopSettings = await getShopByUser(user.uid!);
    if (shopSettings == null) return;
    setState(() {
      shopNameController.text = shopSettings.name ?? '';
      _uid = shopSettings.id ?? const Uuid().v4();
      _cashbackDay = shopSettings.cashBackDay?.toDouble() ?? 1.0;
      _cashbackPercentage = shopSettings.cashBackPercentage ?? 10;
      defaultImage = shopSettings.avatarUrl;
      if (defaultImage != null) {
        _image = File(defaultImage!);
      }
    });
  }

  Future<void> _saveData() async {
    // Save shop settings to Firestore
    final user = FirebaseAuth.instance.currentUser;
    if (user == null) return;
    setState(() => {
      isSaving = true
    });
    String? imageUrl;
    if (_image != null && File(defaultImage!).toString()!=_image?.toString()) {
      final ref = FirebaseStorage.instance
          .ref()
          .child('shops')
          .child(user.uid)
          .child('image.jpg');

      final task = ref.putFile(_image!);
      await task.whenComplete(() async {
        imageUrl = await ref.getDownloadURL();
      });
    }
    else{
      imageUrl = defaultImage;
    }
    ShopSettings newSettings = ShopSettings(id: _uid, avatarUrl: imageUrl, name: shopNameController.text, cashBackDay: _cashbackDay.toInt(), cashBackPercentage: _cashbackPercentage);
    var shopsSettingsSnapshot = await shopSettingsRef.whereDocumentId(isEqualTo: _uid).get();
    if(shopsSettingsSnapshot.docs.isNotEmpty){
      shopSettingsRef.doc(_uid).set(newSettings);
    }
    else{
      shopSettingsRef.add(newSettings);
    }

  }

  Future<void> _getImage(ImageSource source) async {
    final pickedFile = await ImagePicker().pickImage(source: source);
    if (pickedFile != null) {
      setState(() {
        _image = File(pickedFile.path);
      });
    }
    setState(() => {
      isSaving = false
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Настройки магазина'),
        actions: [
          !isSaving ? IconButton(
            icon: const Icon(Icons.save),
            onPressed: () async {
              if (_formKey.currentState!.validate()) {
                await _saveData();
                Navigator.of(context).pop();
              }
            },
          ) : Center(
            child: Container(
                width: 16,
                height: 16,
                child: const CircularProgressIndicator(color: Colors.white),),
          )

        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Название магазина',
                style: TextStyle(fontSize: 16),
              ),
              const SizedBox(height: 8),
              TextFormField(
                controller: shopNameController,
                decoration: const InputDecoration(
                  border: OutlineInputBorder(),
                  hintText: 'Enter shop name',
                ),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Please enter a shop name';
                  }
                  return null;
                },
              ),
              const SizedBox(height: 16),
              Text(
                'Поступление кэшбэка через ${_cashbackDay.toInt()} дней',
                style: const TextStyle(fontSize: 16),
              ),
              const SizedBox(height: 8),
              Slider(
                value: _cashbackDay,
                min: 0,
                max: 30,
                divisions: 30,
                label: '$_cashbackDay',
                onChanged: (value) {
                  setState(() {
                    _cashbackDay = value;
                  });
                },
              ),
              const SizedBox(height: 8),
              Text(
                'Процент кэшбэка ${_cashbackPercentage.toInt()}% с суммы оплаты',
                style: const TextStyle(fontSize: 16),
              ),
              Slider(
                value: _cashbackPercentage.toDouble(),
                min: 0,
                max: 100,
                divisions: 30,
                label: '$_cashbackPercentage',
                onChanged: (value) {
                  setState(() {
                    _cashbackPercentage = value.toInt();
                  });
                },
              ),
              const SizedBox(height: 16),
              // const Text(
              //   'Лого магазина',
              //   style: TextStyle(fontSize: 16),
              // ),
              // const SizedBox(height: 8),
              // if (_image != null)
              //   if(File(defaultImage!).toString()==_image?.toString())
              //     Image.network(defaultImage!)
              //     else if(kIsWeb)
              //     Image.network(_image.!)
              //   else
              //     Image.file(_image!),
              // SizedBox(
              //   width: double.infinity,
              //   child: ElevatedButton(
              //     onPressed: () {
              //       showModalBottomSheet(
              //         context: context,
              //         builder: (BuildContext context) {
              //           return SafeArea(
              //             child: Column(
              //               mainAxisSize: MainAxisSize.min,
              //               children: <Widget>[
              //                 ListTile(
              //                   leading: const Icon(Icons.camera_alt),
              //                   title: const Text('Сделать снимок'),
              //                   onTap: () {
              //                     Navigator.of(context).pop();
              //                     _getImage(ImageSource.camera);
              //                   },
              //                 ),
              //                 ListTile(
              //                   leading: const Icon(Icons.photo_library),
              //                   title: const Text('Выбрать из галереи'),
              //                   onTap: () {
              //                     Navigator.of(context).pop();
              //                     _getImage(ImageSource.gallery);
              //                   },
              //                 ),
              //               ],
              //             ),
              //           );
              //         },
              //       );
              //     },
              //     child: const Text('Выбрать изображение'),
              //   ),
              // ),
              const SizedBox(height: 16,),
              SizedBox(width: double.infinity,
              child: ElevatedButton(
                onPressed: _saveData,
                child: const Text('Сохранить'),
              ),),
              const SizedBox(height: 8,),
              SizedBox(width: double.infinity,
                child: ElevatedButton(
                  style:ElevatedButton.styleFrom(
                   backgroundColor: Colors.red
                  ),
                  onPressed: (){
                    FirebaseAuth.instance.signOut();
                    Navigator.popAndPushNamed(context, '/login');
                  },
                  child: const Text('Выйти'),
                ),)
            ],
          ),
        ),
      ),
    );
  }
}