import 'package:cashback_app/odm/shop_settings/shop_settings.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../odm/cashback/cashback.dart';
import '../../odm/user/user.dart';

class FinishPaymentScreenArgs {
  final String cashBackUserId;
  final int cashBackAmount;

  FinishPaymentScreenArgs(this.cashBackUserId, this.cashBackAmount);
}

class FinishPaymentScreen extends StatefulWidget {
  const FinishPaymentScreen({Key? key}) : super(key: key);

  @override
  State<FinishPaymentScreen> createState() => _FinishPaymentScreenState();
}

class _FinishPaymentScreenState extends State<FinishPaymentScreen> {
  MyUser? client;
  MyUser? shop;
  ShopSettings? shopSettings;
  int balance = 0;
  bool isChargedFromBalance = false;
  int cashBackValue = 0;
  bool isUserFound = true;
  bool success = false;
  @override
  void initState() {
    // TODO: implement initState

    super.initState();

    Future.delayed(Duration(seconds: 0), () async {
      final args = ModalRoute
          .of(context)!
          .settings
          .arguments as FinishPaymentScreenArgs;
      try {
        var users = await usersRef.whereDocumentId(
            isEqualTo: args.cashBackUserId)
            .get();
        if (users.docs.isNotEmpty) {
          setState(() {
            client = users.docs.first.data;
          });
        }
        else {
          setState(() {
            isUserFound = false;
          });
          return;
        }
        var shopSnapshot = await usersRef.whereDocumentId(
            isEqualTo: FirebaseAuth.instance.currentUser?.uid).get();

        if (shopSnapshot.docs.isNotEmpty) {
          setState(() {
            shop = shopSnapshot.docs.first.data;
          });
          var shopSettingsSnapshot = await shopSettingsRef.whereDocumentId(
              isEqualTo: shop!.shopId).get();
          if (shopSettingsSnapshot.docs.isNotEmpty) {
            shopSettings = shopSettingsSnapshot.docs.first.data;
          }
        }
        if (shop != null && client != null) {
          int b = await CashBack.getCashbackBalanceByUserId(
              clientId: client!.id, shopId: shopSettings!.id!);
          setState(() =>
          {
            cashBackValue =
                args.cashBackAmount * shopSettings!.cashBackPercentage! ~/ 100,
            balance = b,

          });
        }
      }
      catch (e){
        setState(() =>
        {
          isUserFound = false
        });
      }
    });
  }



  @override
  Widget build(BuildContext context) {
    final args = ModalRoute
        .of(context)!
        .settings
        .arguments as FinishPaymentScreenArgs;
    // вообще не правильно надо переписать
    if(!isUserFound) {
      return Scaffold(
          appBar: AppBar(title: Text('Начисление кэшбэка')),
          body: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Пользователь не найден', textAlign: TextAlign.center,),
                SizedBox(height: 20,),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(onPressed: (){
                    Navigator.popAndPushNamed(context, '/shop');
                  }, child: Text('Назад')),
                )

              ],
            )));
    }
    if(success) {
      return Scaffold(
          appBar: AppBar(title: Text('Начисление кэшбэка')),
          body: Container(
              alignment: Alignment.center,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Успешный платеж', textAlign: TextAlign.center,),
                  Icon(Icons.check, size: 40, color: Colors.green),
                  SizedBox(height: 20,),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(onPressed: (){
                      Navigator.popAndPushNamed(context, '/shop');
                    }, child: Text('Назад')),
                  )

                ],
              )));
    }

    return Scaffold(
        appBar: AppBar(title: Text('Начисление кэшбэка')),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 70),
          alignment: Alignment.center,
          child: Column(
            children: [
              SizedBox(height: 30,),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('ФИО клиента:'),
                        Text(client?.fullName ?? '')
                      ]
                  ),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Сумма на балансе клиента:'),
                        Text('$balance')
                      ]
                  ),
                  SizedBox(height: 20,),
                  Row(
                    children: [
                      Text('Общая сумма оплаты:'),
                      Text('${args.cashBackAmount}')
                    ],
                  ),
                  if(!isChargedFromBalance)
                  Row(
                    children: [
                      Text('Cумма кэшбэка (${shopSettings?.cashBackPercentage}%):'),
                      Text('$cashBackValue')
                    ],
                  )
                  else
                    Row(
                      children: [
                    Text('Оставшаяся сумма для оплаты:'),
                  Text('${args.cashBackAmount - balance}')
                      ],
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Списать сумму с баланса'),
                      Switch(
                        value: isChargedFromBalance,
                        onChanged: (value) {
                          setState(() {
                            isChargedFromBalance = value;
                          });
                        },
                      ),
                    ],
                  ),

              ElevatedButton(onPressed: () {
                if (isChargedFromBalance) {
                  client!.useCashback(shopId: shopSettings!.id!, paymentFullAmount: args.cashBackAmount,usedCashBackValue: balance);
                }
                else{
                  client!.createCashBack(shopId: shopSettings!.id!, cashBackValue: cashBackValue, paymentFullAmount: args.cashBackAmount, cashBackDay: shopSettings!.cashBackDay!);
                }
                setState(() {
                  success = true;
                });
              },
                  child: Text((isChargedFromBalance)
                      ? 'Списать с баланса'
                      : 'Начислить баланс'))
            ],
          ),
        )
    );
  }
}
