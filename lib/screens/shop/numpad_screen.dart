import 'package:cashback_app/screens/shop/finish_payment_screen.dart';
import 'package:cashback_app/screens/shop/qr_reader_screen.dart';
import 'package:flutter/material.dart';
import 'package:simple_barcode_scanner/simple_barcode_scanner.dart';

class NumPadScreen extends StatefulWidget {
  const NumPadScreen({Key? key}) : super(key: key);

  @override
  _NumPadScreenState createState() => _NumPadScreenState();
}

class _NumPadScreenState extends State<NumPadScreen> {
  String _input = '';

  void _onKeyPressed(String key) {
    setState(() {
      if (key == 'C') {
        // Clear input
        _input = '';
      } else if (key == '<') {
        // Remove last digit
        if (_input.isNotEmpty) {
          _input = _input.substring(0, _input.length - 1);
        }
      } else if (key == '0'){
        if(_input.length!=0){
          _input += key;
        }
      }
      else {
        // Add digit to input
        _input += key;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    /*24 is for notification bar on Android*/
    final double itemHeight = (size.height - kToolbarHeight - 24) / 3.8;
    final double itemWidth = size.width / 2.1;
    return Scaffold(
      appBar: AppBar(
        leading: null,
        title: Text('Введите полную сумму платежа'),
        actions: [
          PopupMenuButton(
            // add icon, by default "3 dot" icon
            // icon: Icon(Icons.book)
              itemBuilder: (context){
                return [
                  PopupMenuItem<int>(
                    value: 0,
                    child: Text("Настройки"),
                  ),

                  PopupMenuItem<int>(
                    value: 1,
                    child: Text("История"),
                  ),
                ];
              },
              onSelected:(value){
                if(value == 0){
                  Navigator.pushNamed(context, '/shop/settings');
                }else if(value == 1){
                  Navigator.pushNamed(context, '/shop/history');
                }
              }
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 24, bottom: 24, left: 24, right: 24),
            child: Text(
              _input,
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 32),
            ),
          ),
          SizedBox(height: 32),
          Expanded(
            flex: 6,
            child: GridView.count(
              shrinkWrap: true,
              childAspectRatio: (itemWidth / itemHeight),
              crossAxisCount: 3,
              children: [
                _buildKey('1'),
                _buildKey('2'),
                _buildKey('3'),
                _buildKey('4'),
                _buildKey('5'),
                _buildKey('6'),
                _buildKey('7'),
                _buildKey('8'),
                _buildKey('9'),
                _buildKey('C', textColor: Colors.red),
                _buildKey('0'),
                _buildKey('<', textColor: Colors.red),
              ],
            ),
          ),

          Expanded(
            flex: 1,
            child: ElevatedButton(onPressed: () async {
              // var res = await Navigator.push(
              //     context,
              //     MaterialPageRoute(
              //       builder: (context) => const SimpleBarcodeScannerPage(),
              //     ));
            Navigator.pushNamed(context, '/shop/finish_payment', arguments: FinishPaymentScreenArgs('cfj4AALyBsa9hcyX8JH0mgGosB72', int.parse(_input)));
          },
            child: Text('Отсканировать QR'),
          ),)

        ],
      ),
    );
  }

  Widget _buildKey(String text, {Color textColor = Colors.black}) {
    return GestureDetector(
      onTap: () => _onKeyPressed(text),
      child: Material(
        color: Colors.transparent,
        child: Ink(
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey),
            borderRadius: BorderRadius.circular(8),
          ),
          child: InkWell(
            borderRadius: BorderRadius.circular(8),
            onTap: () => _onKeyPressed(text),
            child: Container(
              padding: EdgeInsets.all(4),
              height: 100,
              child: Center(
                child: Text(
                  text,
                  style: TextStyle(fontSize: 16, color: textColor),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
