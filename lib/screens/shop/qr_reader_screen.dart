import 'package:cashback_app/screens/shop/finish_payment_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class QrReaderScreenArgs {
  final int cashBackAmount;
  QrReaderScreenArgs(this.cashBackAmount);
}

class QrReaderScreen extends StatefulWidget {
  const QrReaderScreen({Key? key}) : super(key: key);

  @override
  State<QrReaderScreen> createState() => _QrReaderScreenState();
}

class _QrReaderScreenState extends State<QrReaderScreen> {
  @override
  void initState() {

    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {

      // add your code here.
      final args = ModalRoute.of(context)!.settings.arguments as QrReaderScreenArgs;


      Navigator.pushReplacementNamed(context, '/shop/finish_payment',arguments: FinishPaymentScreenArgs('F45TjqiR6KiqZLFjr82I', args.cashBackAmount));
    });



  }
  @override
  Widget build(BuildContext context) {

    return const Placeholder();
  }
}
