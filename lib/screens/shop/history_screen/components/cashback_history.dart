import 'package:cashback_app/odm/cashback/cashback.dart';
import 'package:cashback_app/odm/user/user.dart';
import 'package:flutter/material.dart';


class CashBackHistory extends StatefulWidget {
  final String shopId;

  const CashBackHistory({super.key, required this.shopId});
  @override
  _CashBackHistoryState createState() => _CashBackHistoryState();
}

class _CashBackHistoryState extends State<CashBackHistory> {
  Stream<CashBackQuerySnapshot>? _usersStream;

  @override
  Widget build(BuildContext context) {
    _usersStream = cashBackRef.whereShopId(isEqualTo: widget.shopId).snapshots();
    return StreamBuilder<CashBackQuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<CashBackQuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Text("Loading");
        }

        return ListView(
          children: snapshot.data!.docs.map((CashBackQueryDocumentSnapshot document) {
            var data = document.data;
            MyUser user = MyUser.getUserByUid(data.userId);
            return Card(
              child: ListTile(
                title: Text(data.userId),
                subtitle: Text(data.createdAt.toString()),
                trailing: Text("+${data.cashBackSum}", style: const TextStyle(color: Colors.green),),
              ),
            );
          }).toList(),
        );
      },
    );
  }
}