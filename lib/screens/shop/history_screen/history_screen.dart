import 'package:cashback_app/screens/shop/history_screen/components/cashback_history.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../../../odm/shop_settings/shop_settings.dart';
import '../../../odm/user/user.dart';

class HistoryScreen extends StatefulWidget {
  const HistoryScreen({Key? key}) : super(key: key);

  @override
  State<HistoryScreen> createState() => _HistoryScreenState();
}

class _HistoryScreenState extends State<HistoryScreen> {
  MyUser? shop;
  ShopSettings? shopSettings;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    Future.delayed(Duration(seconds: 0), () async {
      var shopSnapshot = await usersRef
          .whereDocumentId(isEqualTo: FirebaseAuth.instance.currentUser?.uid)
          .whereFieldPath(FieldPath(const ['userType']), isEqualTo: 'shop')
          .get();
      if (shopSnapshot.docs.isNotEmpty) {
        setState(() {
          shop = shopSnapshot.docs.first.data;
        });
        var shopSettingsSnapshot = await shopSettingsRef
            .whereDocumentId(isEqualTo: shop!.shopId)
            .get();
        if (shopSettingsSnapshot.docs.isNotEmpty) {
          setState(() {
            shopSettings = shopSettingsSnapshot.docs.first.data;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('История'),
      ),
      body: (shopSettings != null)
          ? CashBackHistory(
              shopId: shopSettings!.id!,
            )
          : Container(
              alignment: Alignment.center,
              height: MediaQuery.of(context).size.height,
              width: double.infinity,
              child: Text('История пуста'),
            ),
    );
  }
}
