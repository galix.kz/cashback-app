import 'dart:async';

import 'package:cashback_app/odm/user/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ShopLoginScreen extends StatefulWidget {
  @override
  State<ShopLoginScreen> createState() => _ShopLoginScreenState();
}
class _ShopLoginScreenState extends State<ShopLoginScreen> {
  final emailController = TextEditingController(text: 'galix.kz@gmail.com');
  final passwordController = TextEditingController(text: 'galizhan98');
  String error = '';
  Future<void> login() async {
    setState(() {
      error = '';
    });
    try {
      UserCredential userCredential =
          await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: emailController.text,
        password: passwordController.text,
      );

      MyUser? user = await MyUser.getUserByUid(userCredential.user!.uid!);
      if (user == null || user.userType != UserType.shop) {
        setState(() {
          error = 'Пользователь не найден';
        });
        FirebaseAuth.instance.signOut();
      }
      else{
        Navigator.pushReplacementNamed(context, '/shop');
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'user-not-found') {
        setState(() {
          error = 'Пользователь не найден.';
        });
      } else if (e.code == 'wrong-password') {
        setState(() {
          error = 'Неверный пароль.';
        });
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Выберите метод входа'),
        ),
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  error,
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
                TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                    hintText: 'Электронная почта',
                  ),
                ),
                TextField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Пароль',
                  ),
                ),
                SizedBox(height: 20,),
                ElevatedButton(
                  onPressed: login,
                  child: Text('Войти'),
                ),
              ],
            ),
          ),
        ));
  }
}
