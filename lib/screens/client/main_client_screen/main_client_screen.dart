import 'package:cashback_app/screens/client/main_client_screen/components/stores_card_container.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'components/client_info.dart';

class MainClientScreen extends StatefulWidget {
  const MainClientScreen({Key? key}) : super(key: key);

  @override
  State<MainClientScreen> createState() => _MainClientScreenState();
}

class _MainClientScreenState extends State<MainClientScreen> {
  _logout() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, '/');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: const Icon(Icons.logout),
            onPressed: _logout,
          ),
          const SizedBox(width: 10,)
        ],
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: ListView(
          children: [
            UserInfoCard(documentId: FirebaseAuth.instance.currentUser!.uid),
            const SizedBox(height: 30,),
            const SizedBox(
              height: 400,
              child: StoresCardContainer(),
            )

          ],
        ),
      )
    );
  }
}
