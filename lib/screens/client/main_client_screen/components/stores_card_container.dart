import 'package:cashback_app/odm/cashback/cashback.dart';
import 'package:cashback_app/odm/shop_settings/shop_settings.dart';
import 'package:cashback_app/screens/client/main_client_screen/components/store_info_card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class StoresCardContainer extends StatefulWidget {
  const StoresCardContainer({super.key});

  @override
  _StoresCardContainerState createState() => _StoresCardContainerState();
}

class _StoresCardContainerState extends State<StoresCardContainer> {
  Map<String, int>? allCashBacks;
  List<ShopSettings> shops = [];
  _loadData() async {
    Map<String, int>? tempCashbacks = await CashBack.getUsersAllCashBacks(clientId: FirebaseAuth.instance.currentUser!.uid);
    var shopSettingsSnapshot = await shopSettingsRef.get();
    List<ShopSettings> tempShops = [];
    for(var shopSetting in shopSettingsSnapshot.docs) {
      tempShops.add(shopSetting.data);
    }
    setState(() {
      shops = tempShops;
      allCashBacks = tempCashbacks;
    });
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loadData();
  }

  @override
  Widget build(BuildContext context) {

    return ListView.builder(
      itemCount: shops.length,
      itemBuilder: (context, index){
        return StoreInfoCard(
          name: shops[index].name ?? '',
          imageUrl: shops[index].avatarUrl,
          cashBackBalance: allCashBacks?[shops[index].id] ?? 0,
        );
      }
      );

  }
}