import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class StoreInfoCard extends StatelessWidget {
  final String name;
  final int cashBackBalance;
  final String? imageUrl;
  const StoreInfoCard({Key? key, required this.name, required this.cashBackBalance, required this.imageUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    print(imageUrl);
    return Card(
      child: ListTile(
        title: Text(name),
        subtitle: Text('Сумма кэшбэка: $cashBackBalance'),
        leading: (imageUrl!=null) ? CachedNetworkImage(
          imageUrl: imageUrl!,
          progressIndicatorBuilder: (context, url, downloadProgress) =>
              CircularProgressIndicator(value: downloadProgress.progress),
          errorWidget: (context, url, error) => const Icon(Icons.error),
        ) : null,
      ),
    );
  }
}
