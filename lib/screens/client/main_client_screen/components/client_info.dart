import 'package:cashback_app/odm/user/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:qr/qr.dart';
import 'package:qr_flutter/qr_flutter.dart';

class UserInfoCard extends StatelessWidget {
  final String documentId;

  const UserInfoCard({
    super.key,
    required this.documentId,
  });

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<MyUserDocumentSnapshot>(
      future: usersRef.doc(documentId).get(),
      builder: (BuildContext context,
          AsyncSnapshot<MyUserDocumentSnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Text("Something went wrong");
        }

        if (snapshot.hasData && !snapshot.data!.exists) {
          return const Text("Document does not exist");
        }

        if (snapshot.connectionState == ConnectionState.done) {
          return Column(
            children: [
              Card(
                child:  QrImage(
                  data: documentId,
                  version: QrVersions.auto,
                  size: 200,
                ),
              ),
              Card(
                child: ListTile(
                  title: Text('ФИО: ${snapshot.data?.data?.fullName ?? ""}'),
                  subtitle: Text('Номер телефона: ${snapshot.data?.data?.phone ?? ""}'),
                  trailing: IconButton(onPressed: () {  }, icon: Icon(Icons.edit))
                ),
              )
            ],
          );
        }

        return const Text("loading");
      },
    );
  }
}
