import 'package:cashback_app/screens/shop/shop_login_screen.dart';
import 'package:flutter/material.dart';

class LoginRedirectScreen extends StatelessWidget {
  const LoginRedirectScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Выберите метод входа'),
      ),
      body:
        Container(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  width: double.infinity, // установите максимальную ширину
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/login/client');
                    },
                    child: Text('Я клиент'),
                  ),
                ),
                SizedBox(height: 20,),
                SizedBox(
                  width: double.infinity, // установите максимальную ширину
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.pushNamed(context, '/login/shop');
                    },
                    child: Text('Я магазин'),
                  ),
                ),
              ],
            ),
          )
        ),
      );
  }
}
